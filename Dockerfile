FROM ubuntu as intermediate

SHELL ["/bin/bash", "-c"]

## Install git
RUN apt-get update
RUN apt-get install -y git

## Add credentials on build
# ARG SSH_PRIVATE_KEY
# ARG SSH_PUBLIC_KEY

# RUN mkdir /root/.ssh/
# RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa \
#     && echo "${SSH_PUBLIC_KEY}" > /root/.ssh/id_rsa.pub

# RUN chmod 600 /root/.ssh/id_rsa \
#     && chmod 600 /root/.ssh/id_rsa.pub

# ## Make sure your domain is accepted
# RUN touch /root/.ssh/known_hosts
# RUN ssh-keyscan gitlab.juice-world.com >> /root/.ssh/known_hosts

# ARG GIT_REPO
# RUN git clone "${GIT_REPO}"
# RUN cd juice-booster && git checkout makefile


# Container from Docker hub for toolchain

FROM rmildocker/mcuexpresso11

# # Final container

# FROM ubuntu

SHELL ["/bin/bash", "-c"]

## Create a directory for project & setup a shared
## workfolder between the host & docker container
# RUN apt-get update
RUN apt-get install -y make

RUN mkdir -p /usr/src/app
# VOLUME ["/usr/src/app"]
WORKDIR /usr/src/app
RUN cd /usr/src/app

# TODO: 
# COPY --from=intermediate juice-booster /usr/src/app/

## Create directory for toolchain
# RUN mkdir -p /opt/toolchain/mcuxpresso-toolchain

# COPY --from=mcuide /usr/local/mcuxpressoide-10.0.2_411/ide/tools/ /opt/toolchain/mcuxpresso-toolchain

## Add toolchain to PATH
ENV PATH="$PATH:/usr/local/mcuxpressoide-11.6.0_8187/ide/tools/bin"

# RUN cd /usr/src/app/build  && make prep debug

